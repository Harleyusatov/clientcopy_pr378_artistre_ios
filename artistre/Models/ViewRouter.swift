//
//  ViewRouter.swift
//  seed
//
//  Created by Achintha Kahawalage on 2021-03-10.
//

import Foundation
import Combine

enum Roots {
    case login
    case artistHome
    case venueHome
}

class ViewRouter: ObservableObject {
    @Published var currentRoot: Roots = LocalUser.current() == nil ? .login : LocalUser.current()?.type == 1 ? .artistHome : .venueHome
    
    static let shared = ViewRouter()
    
    init() {
        print(currentRoot)
    }
}
