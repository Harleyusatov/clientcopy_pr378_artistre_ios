//
//  RootView.swift
//  seed
//
//  Created by Achintha Kahawalage on 2021-03-18.
//

import Foundation
import Combine
import SwiftUI

struct RootView: View {
    
    @ObservedObject var router: ViewRouter
    
    var body: some View {
            Group {
                containedView(roots: router.currentRoot)
            }
    }
    
    func containedView(roots: Roots) -> AnyView {
        switch roots {
        case .login:
            return AnyView(LoginView())
        case .artistHome:
            return AnyView(LoginView())
        case .venueHome:
            return AnyView(LoginView())
        }
    }
}
