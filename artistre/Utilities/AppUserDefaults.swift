//
//  AppUserDefaults.swift
//  seed
//
//  Created by Achintha Kahawalage on 2021-03-12.
//

import Foundation

struct AppUserDefault {
    
    static let shared = UserDefaults.standard
    
    // FCM token
    static func setFCMToken(token: String) {
        shared.set(token, forKey: "FCM_TOKEN")
        shared.synchronize()
    }
    
    static func getFCMToken() -> String {
        if let token = shared.string(forKey: "FCM_TOKEN") {
            return token
        }
        return ""
    }
    
    // Access token
    static func setAccessToken(token: String) {
        shared.set(token, forKey: "ACCESS_TOKEN")
        shared.synchronize()
    }
    
    static func getAccessToken() -> String {
        if let token = shared.string(forKey: "ACCESS_TOKEN") {
            return token
        }
        return ""
    }
}
