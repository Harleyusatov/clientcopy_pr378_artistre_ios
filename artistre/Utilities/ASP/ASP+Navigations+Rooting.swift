//
//  ASP+Navigations+Rooting.swift
//  Copyright © 2020 ElegantMedia. All rights reserved.
//

//import SwiftUI
//
//extension ApplicationServiceProvider {
//
//    //MARK: Manage User Direction
//    public func manageUserDirection(from vc: UIViewController? = nil, window: UIWindow? = nil) {
//        guard LocalUser.current() != nil else {
////            directToPath(in: .Auth, for: .AuthNC, from: vc, window: window)
//            return
//        }
//        // Set access token to SwaggerClientAPI.customHeaders and iBaseSwift AppConstant.customHeaders
//        setupAccessToken()
////        getRedirectionWithMainInterfaceType(type: ApplicationControl.appMainInterfaceType, window: window)
//    }
//    
//    
//    //MARK: Get ridirection with app main interface type
//    func getRedirectionWithMainInterfaceType(type: MainInterfaceType, from vc: UIViewController? = nil, window: UIWindow? = nil) {
//        switch type {
//        case .Main:
//            directToPath(in: .Main, for: .HomeNC, from: vc, window: window)
//        case .SideMenuNavigations:
//            directToPath(in: .SideMenu, for: .SideMenuConfigurationVC, from: vc, window: window)
//        case .TabBarNavigations:
//            directToPath(in: .TabBar, for: .MainTBC, from: vc, window: window)
//        case .Custom:
//            break
//        }
//    }
//    
//    
//    //MARK: Direct to Main Root window
//    public func directToPath(in sb: Storyboard, for identifier: String, from vc: UIViewController? = nil, window: UIWindow? = nil) {
//        let storyboard = UIStoryboard(name: sb.rawValue, bundle: nil)
//        let topController = storyboard.instantiateViewController(withIdentifier: identifier)
//        
//        appDelegate.setAsRoot(_controller: topController)
//    }
//    
//    //MARK: Get ViewController (VC, TVC, CVC, PC, TBC etc.)
//    public func viewController(in sb: Storyboard, identifier: String) -> UIViewController {
//        let storyboard = UIStoryboard(name: sb.rawValue, bundle: nil)
//        let targetVC = storyboard.instantiateViewController(withIdentifier: identifier)
//        return targetVC
//    }
//    
//}
