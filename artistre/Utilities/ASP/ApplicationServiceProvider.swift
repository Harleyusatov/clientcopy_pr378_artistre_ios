//
//  ApplicationServiceProvider.swift
//  seed
//
//  Created by Achintha Kahawalage on 2/24/21.
//
//
import Foundation
import SwiftyBeaver
import RealmSwift

let ASP = ApplicationServiceProvider.self
 
class ApplicationServiceProvider {
    
    static let shared = ApplicationServiceProvider()
    
    let bundleId = Bundle.main.bundleIdentifier ?? ""
    let deviceId = UIDevice.current.identifierForVendor!.uuidString
//    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    let deviceType = "APPLE"
    let Accept = "application/json"
    
    // MARK: Update this if any Realm property changed happened
    let realmSchemaVersion: UInt64 = 0
    
    // Initial parameters for authenication services (Login / Register)
    var initialAuthParameters: [String: Any] = [:]
    
    public init() {}
    
    // Configure Application
    func configure() {
        
        self.setInitialAuthParameters()
        
//        self.setupNetworkingEnvironmentForAuthentications()
 
        //MARK: Realm Step 1
        self.manageRealmMigration()
        
        //MARK: Realm Step 2
        self.printRealmPath()
    }
    
    
    //MARK: Setup initial auth parameters
    private func setInitialAuthParameters() {
        let dict = [
            "device_id": ASP.shared.deviceId,
            "device_type": ASP.shared.deviceType,
            "device_push_token": AppUserDefault.getFCMToken()
        ]
        
//        initialAuthParameters.updateDictionary(otherValues: dict)
    }
    
    
//    //MARK: Setup networking environment
//    private func setupNetworkingEnvironmentForAuthentications() {
//        AppConstant.baseURL = Constant.URLs.baseUrl
//        AppConstant.RESTfulAPIKey = Constant.APIKeys.RESTful
//        AppConstant.googleAPIKey = Constant.APIKeys.google
//    }
//
    //MARK: Logger
    private func configureLogger() {
        let console = ConsoleDestination() // log to Xcode Console
        log.addDestination(console)
    }
    
    
    //MARK: Realm Migration
    func manageRealmMigration() {
        let config = Realm.Configuration(
            schemaVersion: realmSchemaVersion,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < self.realmSchemaVersion) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        Realm.Configuration.defaultConfiguration = config
        _ = try! Realm()
    }
    
    
    //MARK: Print Realm path
    private func printRealmPath() {
        print("Realm path: \(String(describing: try! Realm().configuration.fileURL))")
    }
    
    
    //MARK: Setup access token
    func setupAccessToken() {
        guard LocalUser.current() != nil else {
            return
        }
        SwaggerClientAPI.customHeaders.updateValue(AppUserDefault.getAccessToken(), forKey: "x-access-token")
    }
    
}
