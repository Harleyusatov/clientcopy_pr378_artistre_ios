//
//  ApplicationManager.swift
//  seed
//
//  Created by Achintha Kahawalage on 2021-03-10.
//

import Foundation
import UIKit
import SwiftyBeaver

let log = SwiftyBeaver.self

class ApplicationManager{
    
    static let shared = ApplicationManager()
    
    let deviceId = UIDevice.current.identifierForVendor!.uuidString
    let deviceType = "APPLE"
    let pushToken = AppUserDefault.getFCMToken()
    
    init(){
        self.configSwiftyBeaver()
    }
    
    func configSwiftyBeaver(){
        let console = ConsoleDestination() //log to xCode console
        let file = FileDestination()
        
        SwiftyBeaver.addDestination(console)
        SwiftyBeaver.addDestination(file)
    }
    
    
    
    //MARK: Setup access token
    func setupAccessToken() {
        guard let localUser = LocalUser.current() else {
            return
        }
        SwaggerClientAPI.customHeaders.updateValue(localUser.accessToken, forKey: "x-access-token")
    }
}
