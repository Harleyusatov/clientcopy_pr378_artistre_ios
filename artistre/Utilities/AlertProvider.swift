//
//  AlertProvider.swift
//  seed
//
//  Created by Achintha Kahawalage on 2021-03-17.
//

import Foundation
import SwiftUI

struct AlertProvider: UIViewControllerRepresentable {
    @Binding var shouldShow: Bool
    
    func makeUIViewController(context: Context) -> CustomAlertController {
        return CustomAlertController()
    }
    
    func updateUIViewController(_ uiViewController: CustomAlertController, context: Context) {
        if shouldShow {
            uiViewController.showAlert(title: "", message: "", action: .init(title: ""))
        }
    }
}

class CustomAlertController: UIViewController {
    var isShowing = false
    
    func showAlert(title: String?, message: String, action: AlertAction) {
        // Prevent displaying multiple times
        guard !isShowing else { return }
        isShowing = true
        
        let alert =  UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: action.title, style: .cancel) { (action) in
            self.isShowing = false
        }
        alert.addAction(cancelAction)
        DispatchQueue.main.async {
            guard let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) else {
                return
            }
            
            window.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
}

struct AlertAction {
    var title: String?
    var style: UIAlertAction.Style
    
    init(title: String, style: UIAlertAction.Style = .default) {
        self.title = title
        self.style = style
    }
}
