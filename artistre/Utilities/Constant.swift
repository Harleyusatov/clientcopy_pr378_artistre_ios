//
//  Constant.swift
//  seed
//
//  Created by Achintha Kahawalage on 2/12/21.
//

import Foundation
import SwiftUI

struct Constant {
    
    //MARK: Manage app environment with release type
    static let appEnvironment: DeploymentEnvironment = .development
    
    //MARK: App environments
    enum DeploymentEnvironment {
        case development
        case staging
        case production
    }
    
    //MARK: Get URLs (Base url etc.)
    enum URLs {
        static let baseUrl = getBaseURL()
    }
    
    static func getCustomHeaders() -> [String: String] {
        return ["x-api-key": Constant.APIKeys.RESTful, "Accept" : "application/json"]
    }
    
    //MARK: Provide base url for current app environment
    static func getBaseURL() -> String {
        switch Constant.appEnvironment {
        case .development:
            return "https://seed.sandbox18.preview.cx/api/v1"
        case .staging:
            return ""
        case .production:
            return "https://"
        }
    }
    
    
    //MARK: Get API keys
    enum APIKeys {
        static let RESTful = Constant.getAPIKey()
        static let google = "AIzaSyDvhCb4KV0JGovYCExaJVAjQYbylqmyQ5k" //"AIzaSyAWBpYdXWTp0OzIvSNIB08d3414nYvA3Pc"
    }
    
    //MARK: Provide API key for current app environment
    static func getAPIKey() -> String {
        switch Constant.appEnvironment {
        case .development:
            return "yCyvqST1b+s3SR4861r3ULb/97jZpvtC1gh8fSIYIjc="
        case .staging:
            return ""
        case .production:
            return ""
        }
    }
    
    //MARK: Character counts
    enum Counts {
        static let passwordCount = 8
        static let nameMinimumCharCount = 2
    }
    
    //MARK: Static contents
    enum AppDetails {
        static let termsUrl = "https://www.elegantmedia.com.au/"
        static let privacyUrl = "https://www.elegantmedia.com.au/privacy-policy/"
        static let aboutUrl = "https://www.elegantmedia.com.au/about-us/"
    }
    
    static var myProfile : User?
    
}

