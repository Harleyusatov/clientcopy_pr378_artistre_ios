//
//  Enum.swift
//  artistre
//
//  Created by Achintha Kahawalage on 2021-05-10.
//

import Foundation
import SwiftUI

enum FormError: String {
    case InvalidEmail = "Enter a valid email"
    case EmptyPassword = "Enter a password"
    case WeakPassword = "Enter a strong password"
    case PasswordsDontMatch = "Passwords don't match"
    case EmptyFields = "All fields required"
    case NoError = ""
    case LimitExceeded = "Maximum limit is 30"
}
