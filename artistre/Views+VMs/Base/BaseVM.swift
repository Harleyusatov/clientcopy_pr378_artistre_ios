//
//  BaseVM.swift
//  seed
//
//  Created by Achintha Kahawalage on 2/13/21.
//

import Foundation
import SwiftyJSON


class BaseVM: NSObject, ValidatorDelegate {
    
    var profile: User?
    
    func handleErrorResponse(_ error: Error?, completion: CompletionHandler) {
        
        var errorMsg = ""
        var statusCode = 0
        if let err = error as? ErrorResponse {
            switch err {
            case .error(let _statusCode, let data, let error):
                statusCode = _statusCode
                if let errorData  = data  {
                    let errorJson = JSON(errorData)
                    errorMsg = errorJson["message"].stringValue
                    
                    if errorMsg.isEmpty {
                        errorMsg = error.localizedDescription
                    }
                }
            }
        } else {
            errorMsg = .ErrorCorrupted
            statusCode = 422
        }
        log.error("Status Code:  \(statusCode) \nError: \(errorMsg)")
        completion(false, statusCode, errorMsg)
    }
    
    //MARK: Proceed with logout web service
    func proceedWithLogout(completion: @escaping CompletionHandler) {
                
        //MARK: 2. Update profile picture web service
        AuthAPI.authGetLogout(accept: "application/json") { (response, error) in
            
        }
    }

}
