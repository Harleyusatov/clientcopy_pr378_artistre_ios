//
//  BaseView.swift
//  seed
//
//  Created by Achintha Kahawalage on 2/13/21.
//

import Foundation
import WebKit
import SwiftUI
import RappleProgressHUD

class BaseView: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, LoadingIndicatorDelegate {
    
//    let baseVM = BaseVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // Hide the keyboard when tap background
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Back navigation
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    
    // Dismiss Top VC
    func dismissTop() {
        dismiss(animated: true, completion: nil)
    }
    
    // Show Temp alert
//    func showTempAlert(vc: UIViewController? = nil, tvc: UITableViewController? = nil) {
//        if vc != nil {
//            AlertProvider(vc: vc!).showAlert(title: .Alert, message: .FeatureNotImplemented, action: AlertAction(title: .Dismiss))
//        } else if tvc != nil {
//            AlertProvider(tvc: tvc!).showAlert(title: .Alert, message: .FeatureNotImplemented, action: AlertAction(title: .Dismiss))
//        }
//    }
    
    // Dismiss navigation bar
    //    func transparentNavBar(){
    //
    //
    //    }
    
    func setDelegatesForTextEditors(tfs: [UITextField] = [], tvs: [UITextView] = []) {
        tfs.forEach({ tf in
            tf.delegate = self
        })
        
        tvs.forEach({ tv in
            tv.delegate = self
        })
    }
    
//    func setUpCustomViewGradient(view: UIView) {
//
//        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
//        view.layer.shadowOpacity = 0.80
//        view.layer.shadowOffset = CGSize(width: 0, height: 0.2)
//        view.clipsToBounds = false
//        view.applyGradient(isTopBottom: true, colorArray: [#colorLiteral(red: 0, green: 0.2823529412, blue: 0.4196078431, alpha: 1),#colorLiteral(red: 0.1254901961, green: 0.6078431373, blue: 0.9058823529, alpha: 0.9),#colorLiteral(red: 0.2431372549, green: 0.8274509804, blue: 0.9882352941, alpha: 0.35),#colorLiteral(red: 0.2549019608, green: 0.8470588235, blue: 0.9960784314, alpha: 0),#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.002704847727)])
//
//    }
    
    
    
    // Load Url in webview
    func loadUrlInWebView(urlString: String, webView: WKWebView) {
        if let url = URL(string: urlString) {
            var urlRequest = URLRequest(url: url)
            urlRequest.cachePolicy = .returnCacheDataElseLoad
            webView.load(urlRequest)
            webView.allowsBackForwardNavigationGestures = true
        }
    }
    
    // Load html string in webview
    func loadHtmlStringInWebView(htmlString: String, webView: WKWebView) -> Void {
        webView.loadHTMLString(htmlString, baseURL: nil)
    }
    
}

//MARK: Logout Process
//extension BaseVC {
//
//    func logoutCurrentUser() {
//
//        let yesAction = AlertAction(title: .Yes)
//        let noAction = AlertAction(title: .No, style: .cancel)
//
//        AlertProvider(vc: self).showAlertWithActions(title: .Confirmation, message: .LogoutConfirmation, actions: [yesAction, noAction], completion: { action in
//            if action.title == .Yes {
//                self.logoutNetworkRequest()
//            } else {
//                // Will dismiss alertView by default
//            }
//        })
//    }
//
    //MARK: Logout network request
//    func logoutNetworkRequest() {
//        self.baseVM.proceedWithLogout(completion: { (status, statusCode, message)  in
//            self.stopLoading()
//            switch status {
//            case true:
//                self.deleteLocalUserAndSetRoot()
//            default:
//                switch statusCode {
//                case 401, 403:
//                    self.deleteLocalUserAndSetRoot()
//                default:
//                    AlertProvider(vc: self).showAlert(title: .Error, message: message, action: AlertAction(title: .Dismiss))
//                }
//            }
//        })
//    }

//    //MARK: Delete local user and set root
    func deleteLocalUserAndSetRoot() {
        // Delete Realm current user
        guard let user = LocalUser.current() else { return }
        RealmService.shared.delete(user)
        SwaggerClientAPI.customHeaders.removeValue(forKey: "x-access-token")

        // Direct to login root
//        ASP.shared.directToPath(in: .UserAuth, for: .AuthNC, from: self)
    }


protocol LoadingIndicatorDelegate {
    func startLoading()
    func startLoadingWithText(label: String)
    func stopLoading()
    func startLoadingWithProgress(current: CGFloat, total:CGFloat)
}

extension LoadingIndicatorDelegate {
    // -------- RappleProgressHUD -------- //
    // Start loading
    func startLoading() {
        RappleActivityIndicatorView.startAnimating()
    }
    
    // Start loading with text
    func startLoadingWithText(label: String) {
        RappleActivityIndicatorView.startAnimatingWithLabel(label)
    }
    
    // Stop loading
    func stopLoading() {
        RappleActivityIndicatorView.stopAnimation()
    }
    
    func startLoadingWithProgress(current: CGFloat, total:CGFloat) {
        RappleActivityIndicatorView.setProgress(current/total)
    }
}

