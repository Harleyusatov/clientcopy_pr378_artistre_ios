//
//  CustomTextField.swift
//  artistre
//
//  Created by Achintha Kahawalage on 2021-05-10.
//

import SwiftUI

struct CustomTextField: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .foregroundColor(Color("#212322"))
            .font(Font.custom("Manrope-SemiBold", size: 14))
            .frame(height: 44)
            .padding(.leading,15)
            .background(Color.white)
            .cornerRadius(2)
            .overlay(RoundedRectangle(cornerRadius: 2.0).stroke(Color("#006562"), lineWidth: 1.0))
            .shadow(color: Color("loginShadow"), radius: 10)
    }
}

