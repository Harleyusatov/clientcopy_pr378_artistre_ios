//
//  CustomGradientButton.swift
//  artistre
//
//  Created by Achintha Kahawalage on 2021-05-10.
//

import SwiftUI

struct CustomGradientButton: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
            configuration
                .label
                .foregroundColor(Color.white)
                .font(Font.custom("Manrope-ExtraBold", size: 14))
                .padding(.horizontal,35)
                .padding()
                .background(LinearGradient(gradient: Gradient(colors: [Color("#18DEDB"),Color("#0ABAB5")]), startPoint: .leading, endPoint: .trailing))
                .frame(height: 44)
                .cornerRadius(2)
                
        }
}
