//
//  LoginVM.swift
//  seed
//
//  Created by Achintha Kahawalage on 2/9/21.
//

import Foundation
import SwiftUI
import Combine


class LoginVM: BaseVM, ObservableObject{
    
    @Published var email = ""
    @Published var isInvalidmail = false
    @Published var emailErrorMessage = ""
    
    @Published var password = ""
    @Published var isInvalidPassword = false
    @Published var passwordErrorMessage = ""
    
    @Published var isAllValid = false
    
    private static let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    private static let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    
    @Published var errored = false
    @Published var error: String = ""
    
    private var cancellables = Set<AnyCancellable>()
    
    override init(){
        super.init()
        loginValid
            .receive(on: RunLoop.main)
            .assign(to: \.isAllValid, on: self)
            .store(in: &cancellables)

        isEmailEmpty
            .receive(on: RunLoop.main)
            .map(\.rawValue)
            .assign(to: \.emailErrorMessage, on: self)
            .store(in: &cancellables)

        isPasswordEmpty
            .receive(on: RunLoop.main)
            .map(\.rawValue)
            .assign(to: \.passwordErrorMessage, on: self)
            .store(in: &cancellables)
    }
    
    func proceedWithLogin(completion: @escaping CompletionHandler){
        
        // Check internet connection
        guard Reachability.isInternetAvailable() else {
                self.errored = true
                self.error = .InternetConnectionOffline
            return
        }
        
        AuthAPI.authPostLogin(accept: "application/json", deviceId: ApplicationManager.shared.deviceId, deviceType: ApplicationManager.shared.deviceType, email: email, password: password) { (response, error) in
            if error != nil {
                //handle error
                self.handleErrorResponse(error, completion: { (status, statusCode, message) in
                    completion(status, statusCode, message)
                    self.errored = true
                    self.error = message
                })
            } else {
                guard let _payloadUser = response?.payload else {
                    return
                }
                LocalUser.saveLoginData(user: _payloadUser)
                guard let accessToken = _payloadUser.accessToken else{ return}
                AppUserDefault.setAccessToken(token: accessToken)
                ApplicationServiceProvider.shared.setupAccessToken()
                print(accessToken)
                
                completion(true, 200, response?.message ?? "Success")
            }
        }
    }
}


extension LoginVM {
    private var isEmailEmpty: AnyPublisher<FormError, Never> {
        $email
            .dropFirst()
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .removeDuplicates()
            .map {
                if !Self.emailPredicate.evaluate(with: $0) {
                    self.isInvalidmail = true
                    return FormError.InvalidEmail
                } else {
                    self.isInvalidmail = false
                    return FormError.NoError
                }
            }
            .eraseToAnyPublisher()
    }
    
    private var isPasswordEmpty: AnyPublisher<FormError, Never> {
        $password
            .dropFirst()
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .removeDuplicates()
            .map {
                if $0.isEmpty {
                    self.isInvalidPassword = true
                    return FormError.EmptyPassword
                } else {
                    self.isInvalidPassword = false
                    return FormError.NoError
                }
            }
            .eraseToAnyPublisher()
    }
    
    private var loginValid: AnyPublisher<Bool, Never> {
        Publishers
            .CombineLatest(isEmailEmpty, isPasswordEmpty)
            .debounce(for: 0.3, scheduler: RunLoop.main)
            .map {
                if $0.0 == .NoError, $0.1 == .NoError {
                    return true
                }
                return false
            }
            .eraseToAnyPublisher()
    }
}

