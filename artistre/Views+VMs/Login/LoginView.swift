//
//  ContentView.swift
//  artistre
//
//  Created by Achintha Kahawalage on 2021-05-10.
//

import SwiftUI

struct LoginView: View {
    
//    init() {
//        UINavigationBar.appearance().barTintColor = .white
//        UINavigationBar.appearance().backgroundColor = .white
////        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
//        UINavigationBar.appearance().shadowImage = UIImage()
//        UINavigationBarAppearance().shadowColor = .red
//
//        UINavigationBar.appearance().titleTextAttributes =
//            [.foregroundColor: UIColor.black, .font:UIFont.systemFont(ofSize: 22)]
//    }
    
    @StateObject var vm = LoginVM()
    @State var isForgotPwTapped = false
    
    var body: some View {
        NavigationView{
            
            ZStack{
                Color.white
                    .edgesIgnoringSafeArea(.all)
                VStack(spacing: 0) {
                    CustomNavigationView(isBackButton: false, title: "Artistre", editButton: false)
                    ScrollView {
                        VStack(spacing: 0){
                            VStack (alignment: .leading,spacing: 8){
                                HStack {
                                    Text("Sign In")
                                        .foregroundColor(Color("#212322"))
                                        .font(.custom("Manrope-ExtraBold", size: 24))
                                    Spacer()
                                }
                                Text("Email")
                                    .foregroundColor(Color("#595F5B"))
                                    .font(.custom("Poppins-Medium", size: 12))
                                    .padding(.top,20)
                                TextField("Email", text: $vm.email)
                                    .textFieldStyle(CustomTextField())
                                
                                Text("Password")
                                    .foregroundColor(Color("#595F5B"))
                                    .font(.custom("Poppins-Medium", size: 12))
                                    .padding(.top,30)
                                TextField("Password", text: $vm.password)
                                    .textFieldStyle(CustomTextField())
                                
                            }.padding()
                            
                            Button(action: {
                                self.isForgotPwTapped = true
                            }, label: {
                                Text("Forgot Password?")
                                    .font(.custom("Manrope-Bold", size: 14))
                                    .foregroundColor(Color("#0ABAB5"))
                            })
                            .padding(.top,35)
                            
                            
                            Button("Sign In") {
                                        print("Button pressed!")
                            }.buttonStyle(CustomGradientButton())
                            .padding(.top,20)
                            
                            HStack(spacing:0){
                                Line()
                                    .stroke(Color("dashColor"), style: StrokeStyle(lineWidth: 1, dash: [2]))
                                    .frame(height: 1)
                                ZStack{
                                    Color("#D8D8D8")
                                    Text("Or Sign in with")
                                        .foregroundColor(Color.white)
                                        .font(Font.custom("Manrope-Medium", size: 14))
                                    
                                }.cornerRadius(9.5)
                                Line()
                                    .stroke(Color("dashColor"), style: StrokeStyle(lineWidth: 1, dash: [2]))
                                    .frame(height: 1)
                            }
                            .padding([.top,.bottom,.horizontal],30)
                            
                            VStack(spacing: 15){
                                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                    Image("btn_signin_Apple")
                                })
                                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                    Image("btn_signin_facebook")
                                })
                                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                    Image("btn_signin_google")
                                })
                            }
                            
                            VStack(spacing: 25){
                            HStack{
                                Text("Don’t have an account ?")
                                    .foregroundColor(Color("#595F5B"))
                                    .font(Font.custom("Manrope-Medium", size: 13))
                                
                                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                    Text("Sign Up")
                                        .foregroundColor(Color("#0ABAB5"))
                                        .font(Font.custom("Manrope-Bold", size: 13))
                                })
                            }
                            
                            HStack{
                                Text("Skip & Discover")
                                    .foregroundColor(Color("#0ABAB5"))
                                    .font(Font.custom("Manrope-Bold", size: 12))
                                
                                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                    Image("ic_signin_skip_arrow")
                                })
                            }
                            }.padding([.top, .bottom], 30)
                            
                        }
                        .background(Color.white)
                        .cornerRadius(5.0)
                        .shadow(color: Color.init("loginShadow"), radius: 10, x: 0.0, y: 0.0)
                        .padding(.all,20)
                    }
                }
            }
            .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.top)
                
//            .navigationBarTitle("asfkmas", displayMode: .inline)
            
//        }
            
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}


struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }

    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }

}
